# Ethereum-basics

This code describes various tools used while writing, compiling and deploying a solidity contract on the ethereum network.
For the purpose of demonstration:<br />
<br />1)I have used the rinkeby test netwok for deployment.
<br />2)ganache-cli for test environmnet.
<br />3)mocha for testing.
<br />4)Infura framework for connecting to rinkeby network.

Flowchart is as follows:

![Flowchart](/Flowchart.png?raw=true "Title")
