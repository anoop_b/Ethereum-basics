const assert = require('assert');
const ganache = require('ganache-cli');
const provider = ganache.provider();
const Web3 = require('web3');

const web3 = new Web3(provider);
const {
    interface,
    bytecode
} = require('../compile');


let accounts;
let inbox;

beforeEach(async () => {
    accounts = await web3.eth.getAccounts();
    inbox = await new web3.eth.Contract(JSON.parse(interface))
        .deploy({
            data: bytecode,
            arguments: ['hello world-inintial argument']
        })
        .send({
            from: accounts[0],
            gas: '1000000'
        });
    inbox.setProvider(provider);
});

//testing each funtionality of contract using mocha framework

describe('Inbox', () => {
    it('deploy', () => {
        assert.ok(inbox.options.address);
    });

    it('initial message exists', async () => {
        const message = await inbox.methods.message().call();
        assert.equal(message, 'hello world-inintial argument');
    });

    it('can change messaeg', async () => {
        await inbox.methods.setMessage('new message').send({
            from: accounts[0]
        });
        const message = await inbox.methods.message().call();
        assert.equal(message, 'new message');

    });
});